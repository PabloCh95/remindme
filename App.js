import React, {Component} from 'react'
import {View, Text, StyleSheet, Image, Button, Alert, ImageBackground, TouchableOpacity,
TextInput, ScrollView } from 'react-native'
import { Video } from 'expo-av'

class remindMe extends Component {

  login(){
    Alert.alert('Login de usuario')
  }

  render(){

    return(
      <ImageBackground source={require('./assets/fondoAbuelo.jpg')} style={styles.container}>
      <ScrollView> 
        <View style={styles.header}>

          <View style={styles.headerLeft}>
              <Image source={require('./assets/abuelos.jpg')} 
              style={styles.logo}></Image>
          </View>

          <View style={styles.headerRight}>
              <Button title="Login" color="skyblue" onPress={this.login}></Button>
          </View>

        </View>
        
        <View style={styles.body}>
          <TouchableOpacity>
          <Text style={styles.textColor}>  BIENVENIDOS </Text>
          <TextInput 
            placeholder="Nombre de usuario"
            placeholderTextoColor='red'
            maxLength={8}
            style={{borderWidth:1, borderColor: 'black', fontWeight: 'bold',padding:3 ,marginTop:10,}} > 
          </TextInput>


          </TouchableOpacity>
        </View>
        

        <View style={styles.footer}>
          <View style={[styles.flex, styles.footerLeft]}>
              <Text style={styles.textColor}>FECHAS</Text>
          </View>
          <View style={[styles.flex, styles.footerRight]}>
              <Text style={styles.textColor}>MEDICAMENTOS</Text>
          </View>
          <View style={[styles.flex, styles.footerCenter]}>
              <Text style={styles.textColor}>GPS</Text>
          </View>
          
        </View>
        </ScrollView> 

      </ImageBackground>
    )

  }

}

const styles = StyleSheet.create({
  container : {
    flex : 1,
    flexDirection : 'column',
    backgroundColor : 'blue'
  },
  header : {
    flex : 20,
    flexDirection : 'row',
    marginTop : 50,
    
  },
  headerLeft : {
    flex : 1,
  },
  headerRight : {
    flex: 0.3,
    marginRight : 10    
  },
  body : {
    flex : 1,
    alignItems : 'center',
    justifyContent : 'center',
    //backgroundColor : 'white',
  },
  logo : {
    width : 200,
    height : 120,
    resizeMode : 'contain',
    borderRadius : 30
  },
  footer : {
    flex : 1,
    flexDirection : 'row',
    marginTop : 60,
    fontSize: 100,
  },
  flex : {
    flex : 1
  },
  footerLeft : {
    alignItems : 'center',
    justifyContent : 'center'
  },
  footerRight : {
    alignItems : 'center',
    justifyContent : 'center'
  },
  footerCenter : {
    alignItems : 'center',
    justifyContent : 'center'
  },
  
  textColor : {
    color : 'black',
    fontWeight: 'bold'

  }
})

export default remindMe